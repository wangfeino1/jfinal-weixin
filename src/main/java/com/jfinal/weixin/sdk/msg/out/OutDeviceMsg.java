/**
 * Copyright (c) 2015-2016, Javen Zhou  (javen205@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.jfinal.weixin.sdk.msg.out;

import com.jfinal.weixin.sdk.msg.in.InMsg;

/**
 * @author Javen
 * 2016年6月8日
 * 参考文档http://iot.weixin.qq.com/wiki/document-2_1.html
 */

public class OutDeviceMsg extends OutMsg{
	
	private String content;
	private String deviceType;
	private String deviceID;
	private String sessionID;
	
	
	public OutDeviceMsg() {
		this.msgType = "device_text";
	}
	
	public OutDeviceMsg(InMsg inMsg) {
		super(inMsg);
		this.msgType = "device_text";
	}
	@Override
	protected void subXml(StringBuilder sb) {
		if (null == content) {
			throw new NullPointerException("content is null");
		}
		if (null == deviceType) {
			throw new NullPointerException("deviceType is null");
		}
		if (null == deviceID) {
			throw new NullPointerException("deviceID is null");
		}
		if (null == sessionID) {
			throw new NullPointerException("sessionID is null");
		}
		sb.append("<DeviceType><![CDATA[").append(deviceType).append("]]></DeviceType>\n");
		sb.append("<DeviceID><![CDATA[").append(deviceID).append("]]></DeviceID>\n");
		sb.append("<SessionID>").append(sessionID).append("</SessionID>\n");
		sb.append("<Content><![CDATA[").append(content).append("]]></Content>\n");
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	
	
	
}

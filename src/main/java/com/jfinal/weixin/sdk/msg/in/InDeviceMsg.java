/**
 * Copyright (c) 2015-2016, Javen Zhou  (javen205@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.jfinal.weixin.sdk.msg.in;

import com.jfinal.weixin.sdk.msg.in.InMsg;

/**
 * @author Javen
 * 2016年6月8日
 * 参考文档http://iot.weixin.qq.com/wiki/document-2_1.html
 */

public class InDeviceMsg extends InMsg{
	
	private String deviceType;
	private String deviceID;
	private String content;
	private String sessionID;
	private String msgID;
	private String openID;
	public InDeviceMsg(String toUserName, String fromUserName, Integer createTime, String msgType) {
		super(toUserName, fromUserName, createTime, msgType);
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getDeviceID() {
		return deviceID;
	}
	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getMsgID() {
		return msgID;
	}
	public void setMsgID(String msgID) {
		this.msgID = msgID;
	}
	public String getOpenID() {
		return openID;
	}
	public void setOpenID(String openID) {
		this.openID = openID;
	}
	
	@Override
	public String toString() {
		return "InDeviceMsg [deviceType=" + deviceType + ", deviceID=" + deviceID + ", content=" + content
				+ ", sessionID=" + sessionID + ", msgID=" + msgID + ", openID=" + openID + "]";
	}
	
	
	
}
